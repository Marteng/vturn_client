import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppFooterComponent implements OnInit {

  date: Date = new Date();
  year: number = this.date.getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

}
