import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameUpcomingComponent } from './game-upcoming/game-upcoming.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { GamePassedComponent } from './game-passed/game-passed.component';
import { GroupOverviewComponent } from './group-overview/group-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    GameUpcomingComponent,
    AppHeaderComponent,
    AppFooterComponent,
    GamePassedComponent,
    GroupOverviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
