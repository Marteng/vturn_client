import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameUpcomingComponent } from './game-upcoming.component';

describe('GameUpcomingComponent', () => {
  let component: GameUpcomingComponent;
  let fixture: ComponentFixture<GameUpcomingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameUpcomingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameUpcomingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
