import {LiveGame} from '../game';

export function getLiveGameTime(liveGame: LiveGame): string {
  let counter = 0;
  let i60m10 = liveGame.time;
  while (i60m10 >= 60) {
    i60m10 = i60m10 - 60;
    counter++;
  }
  let sec = i60m10.toString();
  let min = counter.toString();
  while (min.length < 2) {
    min = '0' + min;
  }
  while (sec.length < 2) {
    sec = '0' + sec;
  }
  return min + ':' + sec;
}

export function getDateFormattedString(date: Date): string[] {
  let minute = date.getMinutes().toString();
  let hour = date.getHours().toString();
  while (minute.length < 2) {
    minute = '0' + minute;
  }
  while (hour.length < 2) {
    hour = '0' + hour;
  }
  return [hour, minute];
}
